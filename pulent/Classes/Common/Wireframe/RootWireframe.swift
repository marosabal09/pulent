//
//  RootWireframe.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import UIKit

class RootWireframe
{
    
    var navigationController: MainNavigationController?
    
    init(window: UIWindow)
    {
        // custom initialization
        
        if let navigation = window.rootViewController as? MainNavigationController {
            navigationController = navigation
        }
    }
    
}
