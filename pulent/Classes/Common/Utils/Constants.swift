//
//  Constants.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import Foundation


struct Constants {
    static let searchUrl = "https://itunes.apple.com/search"
    static let lookupUrl = "https://itunes.apple.com/lookup"
    static let userDefaultsTrackId = "UserDefaultsTrackId"
}
