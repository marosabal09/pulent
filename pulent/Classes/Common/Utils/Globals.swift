//
//  Globals.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import UIKit

func _delay(_ delay:Double, closure:@escaping ()->()) {
    
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func _sb(_ s: String) -> UIStoryboard {
    return UIStoryboard(name: s, bundle: nil)
}

func _sb(_ s: String, _ id: String) -> UIViewController {
    return _sb(s).instantiateViewController(withIdentifier: id)
}
