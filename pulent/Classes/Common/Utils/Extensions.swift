//
//  Extensions.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import UIKit

extension String {
    
    var toInt: Int {
        
        if let value = Int(self) {
            return value
        }
        
        return 0
    }
    
    /// Return de lozalized string from the key
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    /// Return de lozalized string from the key with comment
    func localizedWithComment(comment: String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
    
}

extension UIView {
    
    func cornerRadius(_ radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCornersWithLayerMask(_ cornerRadii: CGFloat, corners: UIRectCorner) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
        return maskLayer
    }
    
    func bindEdgesToSuperview() {
        
        guard let s = superview else {
            preconditionFailure("`superview` nil in bindEdgesToSuperview")
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: s.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: s.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: s.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: s.bottomAnchor).isActive = true
    }
    
}

extension UITableView {
    
    func cleanEmptyRowsSeparator() {
        tableFooterView = UIView()
    }
    
}

extension Dictionary : URLQueryParameterStringConvertible {
    /**
     This computed property returns a query parameters string from the given NSDictionary. For
     example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
     string will be @"day=Tuesday&month=January".
     @return The computed parameters string.
     */
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                              String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                              String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }
    
}

extension URL {
    func appendingQueryParameters(_ parametersDictionary : Dictionary<String, String>) -> URL {
        let URLString : String = String(format: "%@?%@", self.absoluteString, parametersDictionary.queryParameters)
        return URL(string: URLString)!
    }
}

extension DateFormatter {
    
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        return formatter
    }()
}

extension JSONEncoder {
    
    static let `default`: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dataEncodingStrategy = .base64
        encoder.dateEncodingStrategy = .formatted(.iso8601)
        return encoder
    }()
}

extension JSONDecoder {
    
    static let `default`: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dataDecodingStrategy = .base64
        decoder.dateDecodingStrategy = .formatted(.iso8601)
        return decoder
    }()
}

extension JSONDecoder {
    
    // to ensure the top-level is always a JSON object.
    private struct DummyDecodable<T : Decodable> : Decodable {
        enum CodingKeys : CodingKey { case value }
        var value: T
    }
    
    func decode<T : Decodable>(
        _ type: T.Type, fromJSONValue jsonValue: Any
        ) throws -> T {
        
        let jsonObject = [
            DummyDecodable<T>.CodingKeys.value.stringValue: jsonValue
        ]
        
        let data = try JSONSerialization.data(withJSONObject: jsonObject)
        
        return try decode(DummyDecodable<T>.self, from: data).value
    }
}

extension JSONEncoder {
    
    // to ensure the top-level is always a JSON object.
    private struct DummyEncodable<T : Encodable> : Encodable {
        enum CodingKeys : CodingKey { case value }
        var value: T
    }
    
    func encodeToJSONValue<T : Encodable>(_ value: T) throws -> Any {
        
        let data = try encode(DummyEncodable(value: value))
        
        let dict = try JSONSerialization.jsonObject(
            with: data
            ) as! [String: Any]
        
        return dict[DummyEncodable<T>.CodingKeys.value.stringValue]!
    }
}
