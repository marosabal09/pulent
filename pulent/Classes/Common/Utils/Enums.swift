//
//  Enums.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import Foundation

enum RowItem {
    
    case track(SearchItem)
    case search(String)
    
}

enum PlayingState {
    
    case play
    case pause
    case prepare
    case stop
    
}
