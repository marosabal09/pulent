//
//  Searchtrack.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import Foundation
import RealmSwift

struct SearchResults: Codable {
    
    var resultCount: Int
    var results: [SearchItem]
    
}

struct SearchItem: Codable {
    
    var wrapperType: String?
    var kind: String?
    var artistId: Int?
    var collectionId: Int?
    var trackId: Int?
    var artistName: String?
    var collectionName: String?
    var trackName: String?
    var collectionCensoredName: String?
    var trackCensoredName: String?
    var artistViewUrl: String?
    var collectionViewUrl: String?
    var trackViewUrl: String?
    var previewUrl: String?
    var artworkUrl30: String?
    var artworkUrl60: String?
    var artworkUrl100: String?
    var collectionPrice: Double?
    var trackPrice: Double?
    var collectionHdPrice: Double?
    var trackHdPrice: Double?
    var releaseDate: String?
    var collectionExplicitness: String?
    var trackExplicitness: String?
    var discCount: Int?
    var discNumber: Int?
    var trackCount: Int?
    var trackNumber: Int?
    var trackTimeMillis: Int?
    var country: String?
    var currency: String?
    var primaryGenreName: String?
    var contentAdvisoryRating: String?
    var shortDescription: String?
    var longDescription: String?
    
    init() {
        
        artistId = 0
        artistName = ""
        artistViewUrl = ""
        artworkUrl100 = ""
        artworkUrl30 = ""
        artworkUrl60 = ""
        collectionCensoredName = ""
        collectionExplicitness = ""
        collectionHdPrice = 0
        collectionId = 0
        collectionName = ""
        collectionPrice = 0
        collectionViewUrl = ""
        contentAdvisoryRating = ""
        country = ""
        currency = ""
        discCount = 0
        discNumber = 0
        kind = ""
        longDescription = ""
        previewUrl = ""
        primaryGenreName = ""
        releaseDate = ""
        shortDescription = ""
        trackCensoredName = ""
        trackCount = 0
        trackExplicitness = ""
        trackHdPrice = 0
        trackId = 0
        trackName = ""
        trackNumber = 0
        trackPrice = 0
        trackTimeMillis = 0
        trackViewUrl = ""
        wrapperType = ""
        
    }
    
    init(track: Track) {
        
        artistId = track.artistId.value
        artistName = track.artistName
        artistViewUrl = track.artistViewUrl
        artworkUrl100 = track.artworkUrl100
        artworkUrl30 = track.artworkUrl30
        artworkUrl60 = track.artworkUrl60
        collectionCensoredName = track.collectionCensoredName
        collectionExplicitness = track.collectionExplicitness
        collectionHdPrice = track.collectionHdPrice.value
        collectionId = track.collectionId.value
        collectionName = track.collectionName
        collectionPrice = track.collectionPrice.value
        collectionViewUrl = track.collectionViewUrl
        contentAdvisoryRating = track.contentAdvisoryRating
        country = track.country
        currency = track.currency
        discCount = track.discCount.value
        discNumber = track.discNumber.value
        kind = track.kind
        longDescription = track.longDescription
        previewUrl = track.previewUrl
        primaryGenreName = track.primaryGenreName
        releaseDate = track.releaseDate
        shortDescription = track.shortDescription
        trackCensoredName = track.trackCensoredName
        trackCount = track.trackCount.value
        trackExplicitness = track.trackExplicitness
        trackHdPrice = track.trackHdPrice.value
        trackId = track.trackId
        trackName = track.trackName
        trackNumber = track.trackNumber.value
        trackPrice = track.trackPrice.value
        trackTimeMillis = track.trackTimeMillis.value
        trackViewUrl = track.trackViewUrl
        wrapperType = track.wrapperType
        
    }
    
}

class OldSearch: Object {
    
    @objc dynamic var text: String = ""
    
    override static func primaryKey() -> String? {
        return "text"
    }
    
}

class Track: Object {
    
    @objc dynamic var wrapperType: String?
    @objc dynamic var kind: String?
    @objc dynamic var artistName: String?
    @objc dynamic var collectionName: String?
    @objc dynamic var trackName: String?
    @objc dynamic var collectionCensoredName: String?
    @objc dynamic var trackCensoredName: String?
    @objc dynamic var artistViewUrl: String?
    @objc dynamic var collectionViewUrl: String?
    @objc dynamic var trackViewUrl: String?
    @objc dynamic var previewUrl: String?
    @objc dynamic var artworkUrl30: String?
    @objc dynamic var artworkUrl60: String?
    @objc dynamic var artworkUrl100: String?
    @objc dynamic var releaseDate: String?
    @objc dynamic var collectionExplicitness: String?
    @objc dynamic var trackExplicitness: String?
    @objc dynamic var country: String?
    @objc dynamic var currency: String?
    @objc dynamic var primaryGenreName: String?
    @objc dynamic var contentAdvisoryRating: String?
    @objc dynamic var shortDescription: String?
    @objc dynamic var longDescription: String?
    
    var artistId = RealmOptional<Int>()
    var collectionId = RealmOptional<Int>()
    
    let discCount = RealmOptional<Int>()
    let discNumber = RealmOptional<Int>()
    let trackCount = RealmOptional<Int>()
    let trackNumber = RealmOptional<Int>()
    let trackTimeMillis = RealmOptional<Int>()
    
    let collectionPrice = RealmOptional<Double>()
    let trackPrice = RealmOptional<Double>()
    let collectionHdPrice = RealmOptional<Double>()
    let trackHdPrice = RealmOptional<Double>()
    
    @objc dynamic var trackId: Int = 0
    override static func primaryKey() -> String? {
        return "trackId"
    }
    
}
