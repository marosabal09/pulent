//
//  PSearchModuleInterface.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

protocol PSearchModuleInterface
{
    func searchTextDidChange(text: String)
    func inCurrentCellIndex(index: Int)
    func didSelectRowAt(indexPath: IndexPath)
    func viewDidAppear()
    func searchBarTextDidEndEditing(text: String)
}

protocol PSearchModuleDelegate
{

}
