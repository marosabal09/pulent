//
//  PSearchInteractor.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

class PSearchInteractor
{
    weak var presenter: PSearchPresenter?
    var dataManager: PSearchDataManager?
    
    var task: URLSessionDataTask?
    var lastTerm = ""
    
    func getOldSearchs() -> [String]? {
        
        return dataManager?.getAllSearchStrings()
        
    }
    
    func saveSearchString(text: String) -> Bool? {
        
        return dataManager?.saveSearchString(text: text)
        
    }
    
    func offlineSearch(term: String, completion: ((SearchResults?, Error?) -> ())? = nil) {
        
        var searchResult = SearchResults(resultCount: 0, results: [])
        
        guard let objects = dataManager?.getObjects(term: term) else {
            DispatchQueue.main.async {
                self.presenter?.showResults(results: searchResult)
                completion?(searchResult, nil)
            }
            return
        }
        
        let searchItems = objects.map{SearchItem(track: $0)}
        searchResult.resultCount = searchItems.count
        searchResult.results = searchItems
        
        DispatchQueue.main.async {
            self.presenter?.showResults(results: searchResult)
            completion?(searchResult, nil)
        }
    }
    
    func search(term: String, completion: ((SearchResults?, Error?) -> ())? = nil) {
        
        if AppDelegate.instance().isReachable() == false {
            
            offlineSearch(term: term, completion: completion)
            
            return
        }
        
        lastTerm = term
        
        guard var URL = URL(string: Constants.searchUrl) else {return}
        
        let parameters = ["mediaType": "music", "limit": "20", "term": term]
        
        URL = URL.appendingQueryParameters(parameters)
        
        if let task = task, task.state == .running {
            
            task.cancel()
        }
        
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
    
        task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
        
            if (error == nil) {
                
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("🗣 URL Session Task Succeeded: HTTP \(statusCode)")

                guard let data = data, statusCode == 200 else {
                    print("💥 Invalid data")
                    completion?(nil, nil)
                    return
                }
                
                do {
                    var results = try JSONDecoder.default.decode(SearchResults.self, from: data)
                    
                    let items = results.results.filter({ (item) -> Bool in
                        return item.trackId != nil
                    })
                    
                    results.resultCount = items.count
                    results.results = items
                    
                    DispatchQueue.main.async {
                        self?.presenter?.showResults(results: results)
                        let saved = self?.dataManager?.saveResult(results: results.results)
                        print("😄 Result saved \(String(describing: saved))")
                        completion?(results, nil)
                    }
                } catch {
                    print("💥 \(error.localizedDescription)")
                    debugPrint(error)
                    completion?(nil, error)
                }
            } else {
                
                // Failure
                print("💥 URL Session Task Failed: %@", error!.localizedDescription)
                completion?(nil, error!)
            }
            
        }
        
        task?.resume()
        
    }
    
    func fetchNext20(limit: Int, completion: ((SearchResults?, Error?) -> ())? = nil) {
        
        if AppDelegate.instance().isReachable() == false {

            DispatchQueue.main.async {
                let searchResult = SearchResults(resultCount: 0, results: [])
                self.presenter?.addResults(results: searchResult)
                completion?(searchResult, nil)
            }
            
            return
        }
        
        guard var URL = URL(string: Constants.searchUrl) else {return}
        
        let parameters = ["mediaType": "music", "limit": "\(limit)", "term": lastTerm]
        
        URL = URL.appendingQueryParameters(parameters)
        
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            
            if (error == nil) {
                
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("🗣 URL Session Task Succeeded: HTTP \(statusCode)")
                
                guard let data = data, statusCode == 200 else {
                    print("💥 Invalid data")
                    completion?(nil, nil)
                    return
                }
                
                do {
                    
                    var results = try JSONDecoder.default.decode(SearchResults.self, from: data)
                    
                    let items = results.results.filter({ (item) -> Bool in
                        return item.trackId != nil
                    })
                    
                    results.resultCount = items.count
                    results.results = items
                    
                    DispatchQueue.main.async {
                        self?.presenter?.addResults(results: results)
                        let saved = self?.dataManager?.saveResult(results: results.results)
                        print("😄 Result saved \(String(describing: saved))")
                        completion?(results, nil)
                    }
                } catch {
                    print("💥 \(error.localizedDescription)")
                    completion?(nil, error)
                }
            } else {
                
                // Failure
                print("💥 URL Session Task Failed: %@", error!.localizedDescription)
                completion?(nil, error!)
            }
            
        }
        
        task.resume()
        
    }
    
}
