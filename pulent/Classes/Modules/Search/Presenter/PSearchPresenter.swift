//
//  PSearchPresenter.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

class PSearchPresenter
{
    var interactor: PSearchInteractor?
    weak var wireframe: PSearchWireframe?
    var userInterface: PSearchViewInterface?
    
    var results: SearchResults?

    func cleanView() {
        
        results = nil
        userInterface?.populateView(with: [])
        
        showOldSearchs()
        
    }
}

extension PSearchPresenter {
    
    func addResults(results: SearchResults) {
        
        guard var oldResult = self.results, results.resultCount > 0 else {
            return
        }
        
        for item in results.results {
            
            if existsTrack(searchItem: item) == false {
                oldResult.results.append(item)
            }
            
        }
        
        oldResult.resultCount = oldResult.results.count
        
        self.results = oldResult
        
        let rows = oldResult.results.map { RowItem.track($0) }
    
        userInterface?.populateView(with: rows)
        
    }
    
    func existsTrack(searchItem: SearchItem) -> Bool {
        
        guard let items = results?.results, let trackId = searchItem.trackId else {
            return true
        }
        
        for item in items {
            
            if let itemId = item.trackId, itemId == trackId {
                return true
            }
            
        }
        
        return false
        
    }
    
    func showResults(results: SearchResults) {
        
        self.results = results
        
        let rows = results.results.map { RowItem.track($0) }
        
        userInterface?.populateView(with: rows)
        
    }
    
    func showOldSearchs() {
        
        guard let oldSearchs = interactor?.getOldSearchs(), AppDelegate.instance().isReachable() == false, (results == nil || results?.resultCount == 0) else {
            return
        }
        
        let rows = oldSearchs.map{ RowItem.search($0) }
        
        userInterface?.populateView(with: rows)
        
    }
    
}


// MARK: - PSearchModuleInterface methods
// implement module interface here
extension PSearchPresenter: PSearchModuleInterface {
    
    func searchBarTextDidEndEditing(text: String) {
        
        if text.count == 0 {
            return
        }
        
        if let results = results, results.resultCount > 0 {
            _ = interactor?.saveSearchString(text: text)
        }
        
    }

    func viewDidAppear() {
        
        showOldSearchs()
    }
    
    func searchTextDidChange(text: String) {
        
        cleanView()
        
        if text.count < 2 {
            return
        }
        
        if AppDelegate.instance().isReachable() == false {
            interactor?.search(term: text)
        } else {
            let fixText = text.replacingOccurrences(of: " ", with: "+")
            interactor?.search(term: fixText)
        }
    }
    
    func inCurrentCellIndex(index: Int) {
        
        guard let `result` = results else {
            return
        }
        
        if index == result.resultCount - 5 {
            interactor?.fetchNext20(limit: result.resultCount + 20)
        }
        
    }
    
    func didSelectRowAt(indexPath: IndexPath) {
        
        if let oldSearchs = interactor?.getOldSearchs(), AppDelegate.instance().isReachable() == false, (results == nil || results?.resultCount == 0) {
            
            searchTextDidChange(text: oldSearchs[indexPath.row])
            userInterface?.chageSearchBarText(text: oldSearchs[indexPath.row])
            
            return
        }
        
        guard let results = self.results, indexPath.row < results.resultCount, let trackId = results.results[indexPath.row].trackId else {
            return
        }
        
        UserDefaults.standard.set(trackId, forKey: Constants.userDefaultsTrackId)
        UserDefaults.standard.synchronize()
        
        wireframe?.presentDetailsScreen()
        
    }
    
}
