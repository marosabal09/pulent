//
//  PSearchWireframe.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import UIKit

class PSearchWireframe
{
    var rootWireframe: RootWireframe?
    var presenter: PSearchPresenter?
    var viewController: PSearchViewController?
    var detailsWireframe: PDetailsWireframe?
    
    func presentSelfAsRootViewController() {
        
        viewController = _sb("SearchStoryboard", "PSearchViewController") as? PSearchViewController
        
        // view <-> presenter
        self.presenter?.userInterface = self.viewController
        self.viewController?.eventHandler = self.presenter
        
        // present controller
        // *** present self with RootViewController
        rootWireframe?.navigationController?.setViewControllers([viewController!], animated: true)
    }
    
    func presentDetailsScreen() {
        detailsWireframe?.presentSelfFromViewController(viewController: nil)
    }
    
}
