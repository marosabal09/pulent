//
//  PResultTableViewCell.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import UIKit

class PResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var artworkUrl100ImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupView() {
        
        viewContainer.clipsToBounds = true
        viewContainer.layer.masksToBounds = true
        viewContainer.layer.cornerRadius = 4
        
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        
    }

}
