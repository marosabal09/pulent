//
//  PSearchView.h
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

protocol PSearchViewInterface
{
    func populateView(with items: Array<RowItem>)
    func chageSearchBarText(text: String)
}
