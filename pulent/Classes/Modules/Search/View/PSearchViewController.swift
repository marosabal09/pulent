//
//  PSearchViewController.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import UIKit
import Haneke

class PSearchViewController: UIViewController
{
    // MARK: - Outlets
    @IBOutlet weak var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchLabel: UILabel!
    
    // MARK: - Attrs
    var eventHandler: PSearchModuleInterface?
    var rows = Array<RowItem>()
    var toolBar: UIToolbar!
    
    // MARK: - View lifecycle
    
    deinit {
        removeNotifications()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        eventHandler?.viewDidAppear()
        updateView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hideKeyboard()
    }
    
    private func setupView() {
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(hideKeyboard))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton], animated: false)
        
        searchBar.inputAccessoryView = toolBar
        
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        
        tableView.cleanEmptyRowsSeparator()
        
        keyboardNotifications()
        
    }
    
    private func updateView() {
        
        if rows.count == 0 {
            searchBar.becomeFirstResponder()
        }
        
    }
    
    private func moveUpSearchBar() {
        
        topLayoutConstraint.constant = 0
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.searchLabel.alpha = 0
        }
        
    }
    
    private func moveDownSearchBar() {
        
        if rows.count != 0 {
            return
        }
        
        topLayoutConstraint.constant = 80
        self.title = nil
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.searchLabel.alpha = 1
        }
        
    }
    
    private func removeNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func keyboardNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
}

// MARK: - PSearchViewInterface methods

// *** implement view_interface methods here

extension PSearchViewController: PSearchViewInterface {
    
    func populateView(with items: Array<RowItem>) {
        
        rows = items
        tableView.reloadData()
        
    }
    
    
    func chageSearchBarText(text: String) {
        
        searchBar.text = text
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        
    }
    
    
}

// MARK: - Button event handlers

// ** handle UI events here

extension PSearchViewController {
    
    @IBAction func hideKeyboard() {
        
        searchBar.resignFirstResponder()
        
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        
        moveUpSearchBar()
        
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        
        moveDownSearchBar()
        
    }
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension PSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if case .search(_) = rows[indexPath.row] {
            return 40
        }
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = rows[indexPath.row]
        
        eventHandler?.inCurrentCellIndex(index: indexPath.row)
        
        switch row {
            
        case .search(let text):
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
            cell.textLabel?.text = text
            return cell
            
        case .track(let item):
            let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as! PResultTableViewCell
            cell.trackNameLabel.text = item.trackName
            cell.artistNameLabel.text = item.artistName
            if let stringUrl = item.artworkUrl100 {
                if let url = URL(string: stringUrl) {
                    cell.artworkUrl100ImageView.hnk_setImageFromURL(url)
                }
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        eventHandler?.didSelectRowAt(indexPath: indexPath)
    }
    
}

// MARK: - UISearchBarDelegate

extension PSearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        eventHandler?.searchBarTextDidEndEditing(text: searchBar.text ?? "")
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        eventHandler?.searchTextDidChange(text: searchText)
    }
    
}
