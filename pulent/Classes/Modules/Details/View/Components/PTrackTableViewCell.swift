//
//  PTrackTableViewCell.swift
//  pulent
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import UIKit

class PTrackTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageViewPlayer: UIImageView!
    @IBOutlet weak var imageViewPlaying: UIImageView!
    @IBOutlet weak var imageViewSpinner: UIImageView!
    
    var loadingTimer: Timer?
    var playingTimer: Timer?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        imageViewPlayer.image = UIImage(named: "icons8-play_round")
        imageViewPlayer.isHidden = false
        imageViewPlaying.isHidden = true
        imageViewSpinner.isHidden = true
        
        stopLoadingAnimation()
        stopLoadingAnimation()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func setupView() {
        
        backgroundView?.backgroundColor = .clear
        backgroundColor = .clear
        
    }
    
    func prepareForPlay() {
        
        imageViewPlaying.isHidden = false
        imageViewPlayer.image = UIImage(named: "icons8-play_round")
        imageViewPlayer.isHidden = true
        imageViewSpinner.isHidden = false
        
        loadingAnimation()
    }
    
    func pause() {
        
        imageViewPlayer.image = UIImage(named: "icons8-play_round")
        imageViewPlayer.isHidden = false
        imageViewPlaying.isHidden = false
        imageViewSpinner.isHidden = true
        
    }
    
    func play() {
        
        imageViewPlayer.image = UIImage(named: "icons8-pause")
        imageViewPlayer.isHidden = false
        imageViewPlaying.isHidden = false
        imageViewSpinner.isHidden = true
        
        stopLoadingAnimation()
        playAnimation()
    }
    
    func stop() {
        
        imageViewPlayer.image = UIImage(named: "icons8-play_round")
        imageViewPlayer.isHidden = false
        imageViewPlaying.isHidden = true
        imageViewSpinner.isHidden = true
        
        stopLoadingAnimation()
        stopPlayAnimation()
    }
    
    func playAnimation() {
        
        stopPlayAnimation()
        
        playingTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
            
            self.imageViewPlaying.transform = self.imageViewPlaying.transform.rotated(by: CGFloat.pi * 0.5)
            
        }
        
    }
    
    func loadingAnimation() {
        
        stopLoadingAnimation()
        
        loadingTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
            
            self.imageViewSpinner.transform = self.imageViewSpinner.transform.rotated(by: CGFloat.pi * 0.5)
            
        }
        
    }
    
    func stopLoadingAnimation() {
        loadingTimer?.invalidate()
        loadingTimer = nil
    }
    
    func stopPlayAnimation() {
        playingTimer?.invalidate()
        playingTimer = nil
    }
    
}
