//
//  PDetailsViewController.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import UIKit
import Haneke

class PDetailsViewController: UIViewController
{
    // MARK: - Outlets
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var bandLabel: UILabel!
    
    // MARK: - Attrs
    var eventHandler: PDetailsModuleInterface?
    var rows = Array<RowItem>()

    // MARK: - View lifecycle

    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupView()
        eventHandler?.updateView()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    private func setupView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.cleanEmptyRowsSeparator()
        
        artworkImageView.clipsToBounds = true
        artworkImageView.layer.masksToBounds = true
        artworkImageView.layer.cornerRadius = 4
        
    }

}

// MARK: - PDetailsViewInterface methods

// *** implement view_interface methods here

extension PDetailsViewController: PDetailsViewInterface {
    
    func populateView(with items: Array<RowItem>) {

        rows = items
        tableView.reloadData()
        
    }
    
    func updateView(item: SearchItem) {
        
        if let stringUrl = item.artworkUrl100 {
            if let url = URL(string: stringUrl) {
                artworkImageView.hnk_setImageFromURL(url)
            }
        }
        
        albumLabel.text = item.collectionName
        bandLabel.text = item.artistName
        
    }
    
    func pauseAt(indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? PTrackTableViewCell else {
            return
        }
        
        cell.pause()
        
    }
    
    func playAt(indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? PTrackTableViewCell else {
            return
        }
        
        cell.play()
        
    }
    
    
    func stopAt(indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? PTrackTableViewCell else {
            return
        }
        
        cell.stop()
        
    }
    
    
    func prepareForPlay(indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? PTrackTableViewCell else {
            return
        }
        
        cell.prepareForPlay()
        
    }
    
}

// MARK: - Button event handlers

// ** handle UI events here

extension PDetailsViewController {
    
    @IBAction func backAction() {
        eventHandler?.backAction()
    }
    
}

extension PDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let row = rows[indexPath.row]
        
        switch row {
        case .search(_):
            return UITableViewCell()
        case .track(let item):
            let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! PTrackTableViewCell
            cell.titleLabel.text = "\(indexPath.row + 1)  \(item.trackName ?? "")"
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        eventHandler?.didSelectRowAt(indexPath: indexPath)
    }
    
}
