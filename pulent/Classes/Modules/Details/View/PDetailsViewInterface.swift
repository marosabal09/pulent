//
//  PDetailsView.h
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

protocol PDetailsViewInterface
{
    func populateView(with items: Array<RowItem>)
    func updateView(item: SearchItem)
    func pauseAt(indexPath: IndexPath)
    func playAt(indexPath: IndexPath)
    func stopAt(indexPath: IndexPath)
    func prepareForPlay(indexPath: IndexPath)
}
