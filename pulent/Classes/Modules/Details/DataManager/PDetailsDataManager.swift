//
//  PDetailsDataManager.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import RealmSwift

class PDetailsDataManager
{
    
    func getObjects(by collectionId: Int) -> Array<Track> {
        
        do {
            
            let realm = try Realm()
            
            let objects = realm.objects(Track.self).filter("collectionId = \(collectionId)")
            
            return Array(objects)
            
        } catch {
            debugPrint(error)
            return []
        }
        
    }

    func getObject(trackId: String) -> Track? {
        
        do {
            
            let realm = try Realm()
            
            let objects = realm.objects(Track.self).filter("trackId = \(trackId.toInt)")
            
            return objects.first
            
        } catch {
            debugPrint(error)
            return nil
        }
        
    }
    
    func saveResult(results: [SearchItem]) -> Bool {
        
        do {
            
            let realm = try Realm()
            
            for item in results {
                
                if item.trackId == nil {
                    continue
                }
                
                let model = Track()
                model.artistId.value = item.artistId
                model.artistName = item.artistName
                model.artistViewUrl = item.artistViewUrl
                model.artworkUrl100 = item.artworkUrl100
                model.artworkUrl30 = item.artworkUrl30
                model.artworkUrl60 = item.artworkUrl60
                model.collectionCensoredName = item.collectionCensoredName
                model.collectionExplicitness = item.collectionExplicitness
                model.collectionHdPrice.value = item.collectionHdPrice
                model.collectionId.value = item.collectionId
                model.collectionName = item.collectionName
                model.collectionPrice.value = item.collectionPrice
                model.collectionViewUrl = item.collectionViewUrl
                model.contentAdvisoryRating = item.contentAdvisoryRating
                model.country = item.country
                model.currency = item.currency
                model.discCount.value = item.discCount
                model.discNumber.value = item.discNumber
                model.kind = item.kind
                model.longDescription = item.longDescription
                model.previewUrl = item.previewUrl
                model.primaryGenreName = item.primaryGenreName
                model.releaseDate = item.releaseDate
                model.shortDescription = item.shortDescription
                model.trackCensoredName = item.trackCensoredName
                model.trackCount.value = item.trackCount
                model.trackExplicitness = item.trackExplicitness
                model.trackHdPrice.value = item.trackHdPrice
                model.trackId = item.trackId ?? -1
                model.trackName = item.trackName
                model.trackNumber.value = item.trackNumber
                model.trackPrice.value = item.trackPrice
                model.trackTimeMillis.value = item.trackTimeMillis
                model.trackViewUrl = item.trackViewUrl
                model.wrapperType = item.wrapperType
                
                try realm.write {
                    realm.add(model, update: true)
                }
                
            }
            
        } catch {
            debugPrint(error)
            return false
        }
        
        return true
        
    }
    
}
