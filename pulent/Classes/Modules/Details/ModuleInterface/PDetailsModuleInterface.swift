//
//  PDetailsModuleInterface.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation

protocol PDetailsModuleInterface
{
    func backAction()
    func updateView()
    func didSelectRowAt(indexPath: IndexPath)
}

protocol PDetailsModuleDelegate
{

}
