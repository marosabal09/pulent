//
//  PDetailsPresenter.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import Haneke
import AVFoundation

class PDetailsPresenter: NSObject
{
    var interactor: PDetailsInteractor?
    weak var wireframe: PDetailsWireframe?
    var userInterface: PDetailsViewInterface?
    
    var results: SearchResults?
    var audioPlayer: AVAudioPlayer?
    var lastIndexPath: IndexPath?
    var playingState: PlayingState = .stop
    var scheduledTimer: Timer?
    
    // MARK: - PDetailsModuleInterface methods
    // implement module interface here
    
    func updateView(item: SearchItem) {
        
        userInterface?.updateView(item: item)
        
    }
    
    func showResults(results: SearchResults) {
        
        self.results = results
        
        let rows = results.results.map { RowItem.track($0) }
        
        userInterface?.populateView(with: rows)
        
    }
    
    func playPreview(data: Data) {
        
        guard let indexPath = lastIndexPath else {
            return
        }
        
        userInterface?.playAt(indexPath: indexPath)
        
        audioPlayer?.stop()
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            audioPlayer = try AVAudioPlayer(data: data)
            
            scheduledTimer?.invalidate()
            scheduledTimer = nil
            
            scheduledTimer = Timer.scheduledTimer(withTimeInterval: audioPlayer!.duration, repeats: false) { (timer) in
                timer.invalidate()
                if let indexPath = self.lastIndexPath {
                    self.stop(indexPath: indexPath)
                }
            }
            
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
            
        } catch let error {
            print("💥 \(error.localizedDescription)")
            debugPrint(error)
        }
        
    }
    
    func pause(indexPath: IndexPath) {
        
        audioPlayer?.pause()
        userInterface?.pauseAt(indexPath: indexPath)
        playingState = .pause
        
    }
    
    func play(indexPath: IndexPath) {
        
        if case .prepare = playingState {
            stop(indexPath: indexPath)
            return
        }
        
        audioPlayer?.play()
        userInterface?.playAt(indexPath: indexPath)
        playingState = .play
        
    }
    
    func stop(indexPath: IndexPath) {
        
        lastIndexPath = nil
        playingState = .stop
        audioPlayer?.stop()
        audioPlayer = nil
        userInterface?.stopAt(indexPath: indexPath)
    }
    
}

extension PDetailsPresenter: PDetailsModuleInterface {
    
    func backAction() {
        
        if let indexPath = lastIndexPath {
            stop(indexPath: indexPath)
        }
        
        scheduledTimer?.invalidate()
        scheduledTimer = nil
        results = nil
        
        wireframe?.dissmissViewController()
        
    }
    
    
    func updateView() {
        
        guard let trackId = UserDefaults.standard.string(forKey: Constants.userDefaultsTrackId) else {
            return
        }
        
        interactor?.fetchAlbum(by: trackId)
        
        UserDefaults.standard.removeObject(forKey: Constants.userDefaultsTrackId)
        UserDefaults.standard.synchronize()
        
    }
    
    func didSelectRowAt(indexPath: IndexPath) {
        
        if let value = lastIndexPath {
            
            if value == indexPath {
                
                if let audioPlayer = self.audioPlayer, audioPlayer.isPlaying {
                    
                    pause(indexPath: indexPath)
                    
                } else {
                
                    play(indexPath: indexPath)
                    
                }

                return
                
            } else {
                
                stop(indexPath: value)
                
            }
            
        }
        
        lastIndexPath = indexPath
        
        guard let previewUrl = results?.results[indexPath.row].previewUrl,
            let url = URL(string: previewUrl) else {
            return
        }
        
        userInterface?.prepareForPlay(indexPath: indexPath)
        playingState = .prepare
        
        interactor?.downloadContentAt(url: url)
        
    }
    
}
