//
//  PDetailsWireframe.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import UIKit

class PDetailsWireframe
{
    var rootWireframe: RootWireframe?
    var presenter: PDetailsPresenter?
    var viewController: PDetailsViewController?

    func presentSelfFromViewController(viewController: UIViewController?)
    {
        // save reference
        self.viewController = _sb("DetailsStoryboard", "PDetailsViewController") as? PDetailsViewController

        // view <-> presenter
        self.presenter?.userInterface = self.viewController
        self.viewController?.eventHandler = self.presenter

        // present controller
        // *** present self with RootViewController
        rootWireframe?.navigationController?.present(self.viewController!, animated: true, completion: nil)
    }
    
    func dissmissViewController() {
        rootWireframe?.navigationController?.dismiss(animated: true, completion: {
            self.viewController = nil
        })
    }
}
