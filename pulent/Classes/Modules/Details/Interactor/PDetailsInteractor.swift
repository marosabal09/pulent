//
//  PDetailsInteractor.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import Haneke

class PDetailsInteractor
{
    weak var presenter: PDetailsPresenter?
    var dataManager: PDetailsDataManager?
    
    func downloadContentAt(url: URL, completion: ((Data?, Error?) -> ())? = nil) {
        
        Shared.dataCache.fetch(URL: url)
            .onSuccess { (data) in
                
                self.presenter?.playPreview(data: data)
                
                completion?(data, nil)
                
            }
            .onFailure { (error) in
                if let err = error {
                    print("💥 \(err.localizedDescription)")
                }
                
                completion?(nil, error)
        }
        
    }
    
    func fetchAlbum(by trackId: String, completion: ((SearchResults?, Error?) -> ())? = nil) {
        
        guard let entity = dataManager?.getObject(trackId: trackId) else {
            let searchResults = SearchResults(resultCount: 0, results: [])
            presenter?.showResults(results: searchResults)
            completion?(searchResults, nil)
            return
        }
        
        presenter?.updateView(item: SearchItem(track: entity))
        
        if AppDelegate.instance().isReachable() == false {
            
            guard let collectionId = entity.collectionId.value, let objects = dataManager?.getObjects(by: collectionId) else {return}
            
            let searchResults = SearchResults(resultCount: objects.count, results: objects.map{ SearchItem(track: $0) })
            
            presenter?.showResults(results: searchResults)
            completion?(searchResults, nil)
            
        } else {
            
            getOnlineData(collectionId: entity.collectionId.value, completion: completion)
            
        }
        
    }
    
    func getOnlineData(collectionId: Int?, completion: ((SearchResults?, Error?) -> ())? = nil) {
        
        guard var URL = URL(string: Constants.lookupUrl), let collectionId = collectionId, AppDelegate.instance().isReachable() else {
            let searchResults = SearchResults(resultCount: 0, results: [])
            presenter?.showResults(results: searchResults)
            completion?(searchResults, nil)
            return
        }
        
        let parameters = [
            "entity": "song",
            "id": "\(collectionId)"
        ]
        
        URL = URL.appendingQueryParameters(parameters)
        
        var request = URLRequest(url: URL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            
            if (error == nil) {
                
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("🗣 URL Session Task Succeeded: HTTP \(statusCode)")
                
                guard let data = data, statusCode == 200 else {
                    print("💥 Invalid data")
                    completion?(nil, nil)
                    return
                }
                
                do {
                    
                    var results = try JSONDecoder.default.decode(SearchResults.self, from: data)
                    
                    let items = results.results.filter({ (item) -> Bool in
                        return item.trackId != nil
                    })
                    
                    results.resultCount = items.count
                    results.results = items
                    
                    DispatchQueue.main.async {
                        self?.presenter?.showResults(results: results)
                        _ = self?.dataManager?.saveResult(results: results.results)
                        completion?(results, nil)
                    }
                } catch {
                    print("💥 \(error.localizedDescription)")
                    completion?(nil, error)
                }
            }
            else {
                
                // Failure
                print("💥 URL Session Task Failed: %@", error!.localizedDescription)
                completion?(nil, error!)
            }
            
        }
        
        task.resume()
    }
    
}
