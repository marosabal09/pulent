//
//  AppDependencies.swift
//  Pulent
//
//  Created by @Ale on 26/07/18.
//
//

import Foundation
import UIKit

class AppDependencies
{

    var searchWireframe: PSearchWireframe!

    func configureDependencies(with window: UIWindow)
    {
        // -----
        // root classes
        let rootWireframe = RootWireframe(window: window)
        // *** add datastore

        // *** module initialization
        
        // ------------------------------------------------------------------
        // begin Search module
        // instantiate classes
        searchWireframe = PSearchWireframe()
        let searchPresenter: PSearchPresenter      = PSearchPresenter()
        let searchDataManager: PSearchDataManager  = PSearchDataManager()
        let searchInteractor: PSearchInteractor    = PSearchInteractor()
        // end Search module
        // ------------------------------------------------------------------
        
        // ------------------------------------------------------------------
        // begin Details module
        // instantiate classes
        let detailsWireframe: PDetailsWireframe      = PDetailsWireframe()
        let detailsPresenter: PDetailsPresenter      = PDetailsPresenter()
        let detailsDataManager: PDetailsDataManager  = PDetailsDataManager()
        let detailsInteractor: PDetailsInteractor    = PDetailsInteractor()
        // end Details module
        // ------------------------------------------------------------------
        
        // ------------------------------------------------------------------
        // begin Search module
        // presenter <-> wireframe
        searchPresenter.wireframe = searchWireframe
        searchWireframe.presenter = searchPresenter
        // presenter <-> interactor
        searchPresenter.interactor = searchInteractor
        searchInteractor.presenter = searchPresenter
        // interactor -> data_manager
        searchInteractor.dataManager = searchDataManager
        // data_manager -> data_store
        // *** connect datastore
        // connect wireframes
        searchWireframe.rootWireframe = rootWireframe
        // *** connect more wireframes
        searchWireframe.detailsWireframe = detailsWireframe
        // configure delegate
        // *** add delegate here if needed
        // end Search module
        // ------------------------------------------------------------------
        
        // --------------------------------------------------------
        // presenter <-> wireframe
        detailsPresenter.wireframe = detailsWireframe
        detailsWireframe.presenter = detailsPresenter
        // presenter <-> interactor
        detailsPresenter.interactor = detailsInteractor
        detailsInteractor.presenter = detailsPresenter
        // interactor -> data_manager
        detailsInteractor.dataManager = detailsDataManager
        // data_manager -> data_store
        // *** connect datastore
        // connect wireframes
        detailsWireframe.rootWireframe = rootWireframe
        // *** connect more wireframes
        // configure delegate
        // *** add delegate here if needed
        // end Details module

        searchWireframe.presentSelfAsRootViewController()
    }
}
