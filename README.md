# Gestor de dependencias
- [Carthage](https://github.com/Carthage/Carthage)

# Base de datos
- [Realm](https://realm.io/docs/swift/latest)

# Cache
- [Haneke](https://github.com/Haneke/HanekeSwift)

# Arquitectura
- [VIPER](https://medium.com/@smalam119/viper-design-pattern-for-ios-application-development-7a9703902af6)

# Instrucciones
- Para compilar el proyecto
    - Abrir el `Terminal` de Mac
    - Ir hasta la raíz del proyecto
    - Ejecutar `carthage update --platform iOS`

# Uso de la arquitectura VIPER
- Desde mi experiencia, llevo tiempo trabajando con esta arquitectura, aunque crea ficheros masivamente facilita la implementación de muchos patrones de diseño y de asignación de responsabilidades, es fácil de mantener, permite la escalabilidad y las pruebas

# Uso de bilbiotecas
- Realm, Hanake
    - Muy fáciles de configurar y utilizar, hacen su trabajo a la perfección. Arriba están los links con la documentación

# Uso del gestor de dependencias
- Carthage
    - Desde mi experiencia es muy conveniente de usar porque solo construye las dependencias una sola vez, es fácil de entender, de mantener y es muy flexible

# Desafío
- Dividir el problema por partes, para determinar un proceso de desarrollo que permita la escalabilidad del proyecto y sea fácil de mantener
- Selección de las tecnologías a utilizar (bibliotecas, gestor de repositorios, control de versiones, arquitectura...)
- Instalación de las bibliotecas y configuración del proyecto para el uso de la arquitectura seleccionada
- Maquetación de las vistas
- Implementación de funcionalidades (por módulos en VIPER) y las pruebas unitarias
- Refactorización de código

# Limitaciones
- En mi caso, el poco acceso a internet, solo tenia disponible 3 ~ 4 horas de internet para hacer el ejercicio