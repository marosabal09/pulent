//
//  pulentUITests.swift
//  pulentUITests
//
//  Created by @Ale on 7/26/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest

class pulentUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOnline() {
        
        
        let app = XCUIApplication()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .searchField).element.tap()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Californication"]/*[[".cells.staticTexts[\"Californication\"]",".staticTexts[\"Californication\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["1  Slip of the Tongue"]/*[[".cells.staticTexts[\"1  Slip of the Tongue\"]",".staticTexts[\"1  Slip of the Tongue\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.buttons["close"].tap()
        
        
    }
    
    func testOffLine() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let sKey = app/*@START_MENU_TOKEN@*/.keys["s"]/*[[".keyboards.keys[\"s\"]",".keys[\"s\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sKey.tap()
        sKey.tap()
        
        let bKey = app/*@START_MENU_TOKEN@*/.keys["b"]/*[[".keyboards.keys[\"b\"]",".keys[\"b\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        bKey.tap()
        bKey.tap()
        
        let tablesQuery = app.tables
        let nastySongFeatSammieLeighBushStaticText = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Nasty Song (feat. Sammie (Leigh Bush))"]/*[[".cells.staticTexts[\"Nasty Song (feat. Sammie (Leigh Bush))\"]",".staticTexts[\"Nasty Song (feat. Sammie (Leigh Bush))\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        nastySongFeatSammieLeighBushStaticText.tap()
        nastySongFeatSammieLeighBushStaticText.tap()
        nastySongFeatSammieLeighBushStaticText.tap()
        
        let staticText = tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["1  Nasty Song (feat. Sammie (Leigh Bush))"]/*[[".cells.staticTexts[\"1  Nasty Song (feat. Sammie (Leigh Bush))\"]",".staticTexts[\"1  Nasty Song (feat. Sammie (Leigh Bush))\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        staticText.tap()
        staticText.tap()
        staticText.tap()
        app.buttons["close"].tap()
        
    }
    
}
