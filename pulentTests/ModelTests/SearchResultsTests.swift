//
//  SearchResultsTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class SearchResultsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testKeepValueAssigned() {
        
        var model = SearchResults(resultCount: 0, results: [])
        
        model.resultCount = 20
        
        XCTAssertEqual(model.resultCount, 20, "the model should keep the value assigned")
    }
    
}
