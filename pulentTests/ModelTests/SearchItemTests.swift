//
//  SearchItemTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class SearchItemTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testKeepValueAssigned() {
        
        var model = SearchItem()
        
        let randomString = UUID().uuidString
        let randomInt = Int(arc4random_uniform(8))
        
        model.artistName = randomString
        model.trackId = randomInt
        
        XCTAssertEqual(model.artistName, randomString, "the model should keep the value assigned")
        XCTAssertEqual(model.trackId, randomInt, "the model should keep the value assigned")
        
    }
    
}
