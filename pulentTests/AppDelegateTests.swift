//
//  AppDelegateTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
import UIKit
@testable import pulent

class AppDelegateTests: XCTestCase {
    
    var appDelegate = AppDelegate()
    var window: UIWindow? = UIWindow()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        appDelegate.window = window
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        window = nil
    }
    
    // MARK: Test after init
    
    func testThatHasInitializerAfterInit() {
        XCTAssertNotNil(appDelegate.dependencies, "should have a PulentAppDependencies after being initialized")
    }
    
    // MARK: Test after application launch
    
    func testWindowIsKeyAfterApplicationLaunch() {
        
        let mainAppDelegate = UIApplication.shared.delegate
        
        if let mainAppDelegate = mainAppDelegate {
            
            if let window = mainAppDelegate.window {
                
                if let window = window {
                    
                    XCTAssertTrue(window.isKeyWindow)
                    
                } else {
                    
                    XCTFail("app delegate window should not be nil")
                    
                }
                
            } else {
                
                XCTFail("app delegate window should not be nil")
                
            }
        } else {
            
            XCTFail("shared application should have a delegate")
            
        }
    }
    
    func testThatDidFinishLaunchingWithOptionsReturnsTrue() {
        XCTAssertTrue(appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil), "should return true from didFinishLaunchingWithOptions")
    }
    
}
