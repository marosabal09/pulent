//
//  PDetailsViewControllerTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PDetailsViewControllerTests: XCTestCase {
    
    class MockPresenter: PDetailsPresenter {
        
    }
    
    var viewController: PDetailsViewController?
    let eventHandler = MockPresenter()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        viewController = _sb("DetailsStoryboard", "PDetailsViewController") as? PDetailsViewController
        viewController?.eventHandler = eventHandler
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewControllerExists() {
        
        XCTAssertNotNil(viewController, "a PDetailsViewController instance should be creatable from storyboard")
        
    }
    
    func testAlbumImageViewExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.artworkImageView, "a artworkImageView should exists")
        
    }
    
    func testAlbumLabelExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.albumLabel, "a albumLabel should exists")
        
    }
    
    func testBandLabelExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.bandLabel, "a bandLabel should exists")
        
    }
    
    func testTableViewExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.tableView, "a TableView should exists")
        
    }
    
}
