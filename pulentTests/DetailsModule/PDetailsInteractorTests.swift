//
//  PDetailsInteractorTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PDetailsInteractorTests: XCTestCase {
    
    class MockDataManager: PDetailsDataManager {
        
        
        
    }
    
    class MockPresenter: PDetailsPresenter {
        
        var updateViewCalled = false
        var showResultsCalled = false
        var playPreviewCalled = false
        
        override func updateView(item: SearchItem) {
            updateViewCalled = true
        }
        
        override func showResults(results: SearchResults) {
            showResultsCalled = true
            self.results = results
        }
        
        override func playPreview(data: Data) {
            playPreviewCalled = true
        }
        
    }
    
    let interactor = PDetailsInteractor()
    let presenter = MockPresenter()
    let dataManager = MockDataManager()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        interactor.presenter = presenter
        interactor.dataManager = dataManager
        presenter.interactor = interactor
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDownloadContent() {
        
        presenter.playPreviewCalled = false
        
        let previewUrl = "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/Music6/v4/13/22/67/1322678b-e40d-fb4d-8d9b-3268fe03b000/mzaf_8818596367816221008.plus.aac.p.m4a"
        let url = URL(string: previewUrl)!
        
        let downloadExpetation = self.expectation(description: "itunes search")
        
        var downloadData: Data?
        var downloadError: Error?
        
        interactor.downloadContentAt(url: url) { (data, error) in
            
            downloadData = data
            
            downloadError = error
            
            downloadExpetation.fulfill()
            
        }
        
        self.waitForExpectations(timeout: 60) { (error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertNil(downloadError, downloadError!.localizedDescription)
            
            XCTAssertTrue(self.presenter.playPreviewCalled, "this method should be called after fetchAlbum request")
            
            XCTAssertNotNil(downloadData, "this value should not be empty after a fetchAlbum request")
            
        }
        
    }
    
    func testOnlineFetch() {
        
        interactor.presenter?.results = nil
        presenter.showResultsCalled = false
        
        let realCollectionId = 879273552
        
        let responseExpectation = self.expectation(description: "itunes search")
        var responseError: Error?
        
        interactor.getOnlineData(collectionId: realCollectionId) { (_, error) in
            
            responseError = error
            responseExpectation.fulfill()
            
        }
        
        self.waitForExpectations(timeout: 60) { (error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertNil(responseError, responseError!.localizedDescription)
            
            XCTAssertTrue(self.presenter.showResultsCalled, "this method should be called after fetchAlbum request")
            
            XCTAssertNotNil(self.presenter.results, "this value should not be empty after a fetchAlbum request")
            
        }
        
    }
    
    func fetchAlbumTest() {
        
        presenter.updateViewCalled = false
        presenter.showResultsCalled = false
        
        interactor.fetchAlbum(by: "some string") { (_, _) in
            
            XCTAssertTrue(self.presenter.showResultsCalled, "this method should be called after fetchAlbum request")
            XCTAssertNotNil(self.presenter.results, "this value should not be empty after a fetchAlbum request")
            XCTAssertEqual(self.presenter.results!.resultCount, 0, "this value should be empty if trackId does not exists")
            
        }
        
    }
    
    func testFetchAlbumForExistValue() {
        
        let randomString = UUID().uuidString
        let randomTrackIdInt = Int(arc4random_uniform(8))
        let randomCollectionIdInt = Int(arc4random_uniform(8))
        
        var model = SearchItem()
        
        model.artistName = randomString
        model.trackId = randomTrackIdInt
        model.collectionId = randomCollectionIdInt
        
        XCTAssert(dataManager.saveResult(results: [model]), "should save data correctly")
        
        interactor.fetchAlbum(by: "\(randomTrackIdInt)") { (results, error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertTrue(self.presenter.updateViewCalled, "this method should be if objects exists")
            
            XCTAssertNotNil(results, "this objects should not be nil")
            
            XCTAssertTrue(self.presenter.showResultsCalled, "this method should be called after fetchAlbum request")
            
            XCTAssert(results!.resultCount > 0, "should retrieve data saved correctly")
            
        }
        
    }
    
}
