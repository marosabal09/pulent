//
//  PDetailsPresenterTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PDetailsPresenterTests: XCTestCase {
    
    class MockInteractor: PDetailsInteractor {
    
        override func fetchAlbum(by trackId: String, completion: ((SearchResults?, Error?) -> ())?) {
            
            let searchResults = SearchResults(resultCount: 0, results: [])
            
            presenter?.showResults(results: searchResults)
            completion?(searchResults, nil)
        }
        
    }
    
    class MockUserInterface: PDetailsViewInterface {
        
        var populateViewCalled = false
        var updateViewCalled = false
        var pauseAtCalled = false
        var playAtCalled = false
        var stopAtCalled = false
        var prepareForPlayCalled = false
        
        func populateView(with items: Array<RowItem>) {
            populateViewCalled = true
        }
        
        func updateView(item: SearchItem) {
            updateViewCalled = true
        }
        
        func pauseAt(indexPath: IndexPath) {
            pauseAtCalled = true
        }
        
        func playAt(indexPath: IndexPath) {
            playAtCalled = true
        }
        
        func stopAt(indexPath: IndexPath) {
            stopAtCalled = true
        }
        
        func prepareForPlay(indexPath: IndexPath) {
            prepareForPlayCalled = true
        }
        
    }
    
    class MockWireframe: PDetailsWireframe {
        
        var dissmissViewControllerCalled = false
        
        override func dissmissViewController() {
            dissmissViewControllerCalled = true
        }
        
    }
    
    let presenter = PDetailsPresenter()
    let interactor = MockInteractor()
    let userInterface = MockUserInterface()
    let wireframe = MockWireframe()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        presenter.userInterface = userInterface
        
        interactor.presenter = presenter
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUpdateViewWithItemCalled() {
        
        userInterface.updateViewCalled = false
        
        presenter.updateView(item: SearchItem())
        
        XCTAssertTrue(userInterface.updateViewCalled, "PDetailsViewInterface.updateView should be called")
        
    }
    
    func testShowResultsCalled() {
        
        userInterface.populateViewCalled = false
        
        presenter.showResults(results: SearchResults(resultCount: 0, results: []))
        
        XCTAssertTrue(userInterface.populateViewCalled, "PDetailsViewInterface.populateViewCalled should be called")
        
    }
    
    func testUpdateViewCalled() {
        
        let randomString = UUID().uuidString
        
        presenter.results = nil
        
        UserDefaults.standard.set(randomString, forKey: Constants.userDefaultsTrackId)
        UserDefaults.standard.synchronize()
        
        presenter.updateView()
        
        XCTAssertNotNil(presenter.results, "should return empty if there is no items with this term or there not exist in the data manager")
        
        XCTAssertEqual(presenter.results!.resultCount, 0, "should return empty if there is no items with this term or there not exist in the data manager")
        
        UserDefaults.standard.removeObject(forKey: Constants.userDefaultsTrackId)
        UserDefaults.standard.synchronize()
        
    }
    
    func backActionCalled() {
        
        presenter.backAction()
        
        XCTAssertTrue(wireframe.dissmissViewControllerCalled, "PDetailsWireframe.dissmissViewController should be called")
        
    }
    
    func testPauseCalled() {
        
        userInterface.pauseAtCalled = false
        
        presenter.pause(indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(userInterface.pauseAtCalled, "PDetailsViewInterface.populateViewCalled should be called")
        
        XCTAssertEqual(presenter.playingState, .pause, "playingState should be on pause")
        
    }
    
    func playCalled() {
        
        userInterface.playAtCalled = false
        
        presenter.pause(indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(userInterface.playAtCalled, "PDetailsViewInterface.playAtCalled should be called")
        
        XCTAssertEqual(presenter.playingState, .play, "playingState should be on play")
        
    }
    
    func stopCalled() {
        
        userInterface.stopAtCalled = false
        
        presenter.pause(indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(userInterface.stopAtCalled, "PDetailsViewInterface.stopAtCalled should be called")
        
        XCTAssertEqual(presenter.playingState, .stop, "playingState should be on stop")
        
        XCTAssertNil(presenter.lastIndexPath, "After stop playing this value should be nil")
        
        XCTAssertNil(presenter.audioPlayer, "After stop playing this value should be nil")
        
    }
    
    // TODO
    func didSelectRowAtCalled() {
        
    }
    
}
