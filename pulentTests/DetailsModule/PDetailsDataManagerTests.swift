//
//  PDetailsDataManagerTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PDetailsDataManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSaveData() {
        
        let dataManager = PDetailsDataManager()
        
        let randomString = UUID().uuidString
        let randomInt = Int(arc4random_uniform(8))
        
        var model = SearchItem()
        
        model.artistName = randomString
        model.trackId = randomInt
        
        XCTAssert(dataManager.saveResult(results: [model]), "should save data correctly")
        
    }
    
    func testRetrieveData() {
        
        let dataManager = PDetailsDataManager()
        
        let randomString = UUID().uuidString
        let randomTrackIdInt = Int(arc4random_uniform(8))
        let randomCollectionIdInt = Int(arc4random_uniform(8))
        
        var model = SearchItem()
        
        model.artistName = randomString
        model.trackId = randomTrackIdInt
        model.collectionId = randomCollectionIdInt
        
        XCTAssert(dataManager.saveResult(results: [model]), "should save data correctly")
        
        let objects = dataManager.getObjects(by: randomCollectionIdInt)
        
        XCTAssert(objects.count > 0, "should retrieve data by collectionId saved correctly")
        
        let track = dataManager.getObject(trackId: "\(randomTrackIdInt)")
        
        XCTAssertNotNil(track, "should retrieve data by trackId saved correctly")
        
    }
    
}
