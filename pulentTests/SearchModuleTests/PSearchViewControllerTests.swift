//
//  PSearchViewControllerTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PSearchViewControllerTests: XCTestCase {
    
    class MockPresenter: PSearchPresenter {
        
        
    }
    
    var viewController: PSearchViewController?
    let eventHandler = MockPresenter()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        viewController = _sb("SearchStoryboard", "PSearchViewController") as? PSearchViewController
        viewController?.eventHandler = eventHandler
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewControllerExists() {
        
        XCTAssertNotNil(viewController, "a PSearchViewController instance should be creatable from storyboard")
        
    }
    
    func testSearchBarExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.searchBar, "a SearchBar should exists")
        
    }
    
    func testTableViewExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.tableView, "a TableView should exists")
        
    }
    
    func testLabelExists() {
        
        _ = viewController?.view
        
        XCTAssertNotNil(viewController?.tableView, "a Label should exists")
        
    }
    
    func testThatAfterSearchBarSearchButtonClicked() {
        
         _ = viewController?.view
        
        viewController?.searchBarSearchButtonClicked(UISearchBar())
        
        if let viewController = viewController {
            if let searchBar = viewController.searchBar {
                XCTAssertFalse(searchBar.isFirstResponder, "shouldn't be first responder anymore")
            }
        }
        
    }
    
}
