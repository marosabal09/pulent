//
//  PSearchDataManagerTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PSearchDataManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSaveData() {
        
        let dataManager = PSearchDataManager()
        
        let randomString = UUID().uuidString
        let randomInt = Int(arc4random_uniform(8))
        
        var model = SearchItem()
        
        model.artistName = randomString
        model.trackId = randomInt
        
        XCTAssert(dataManager.saveResult(results: [model]), "should save data correctly")
        
    }
    
    func testRetrieveData() {
        
        let dataManager = PSearchDataManager()
        
        let randomString = UUID().uuidString
        let randomInt = Int(arc4random_uniform(8))
        
        var model = SearchItem()
        
        model.artistName = randomString
        model.trackId = randomInt
        
        XCTAssert(dataManager.saveResult(results: [model]), "should save data correctly")
        
        let results = dataManager.getObjects(term: randomString)
        
        XCTAssert(results.count > 0, "should retrieve data saved correctly")
        
    }
    
    func testSaveSearchString() {
        
        let dataManager = PSearchDataManager()
        
        let randomString = UUID().uuidString
        
        XCTAssert(dataManager.saveSearchString(text: randomString), "should save data correctly")
        
    }
    
    func testRetriveSearchStrings() {
        
        let dataManager = PSearchDataManager()
        
        let randomString = UUID().uuidString
        
        XCTAssert(dataManager.saveSearchString(text: randomString), "should save data correctly")
        
        let results = dataManager.getAllSearchStrings()
        
        XCTAssert(results.count > 0, "should retrieve data saved correctly")
        
    }
    
}
