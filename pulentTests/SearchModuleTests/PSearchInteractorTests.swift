//
//  PSearchInteractorTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
import RealmSwift
@testable import pulent

class PSearchInteractorTests: XCTestCase {
    
    class MockDataManager: PSearchDataManager {
        
        func createMockData() {
            
            removeAllSearchs()
            
            let randomString = UUID().uuidString
            
            _ = saveSearchString(text: randomString)
            
        }
        
        func removeAllSearchs() {
            
            do {
                
                let realm = try Realm()
                
                let objects = realm.objects(OldSearch.self)
                
                try realm.write {
                    realm.delete(objects)
                }
                
            } catch {
                debugPrint(error)
            }
            
        }
        
    }
    
    class MockPresenter: PSearchPresenter {
        
        
    }
    
    let interactor = PSearchInteractor()
    let presenter = MockPresenter()
    let dataManager = MockDataManager()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        interactor.presenter = presenter
        interactor.dataManager = dataManager
        presenter.interactor = interactor
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearch() {
        
        interactor.presenter?.results = nil
        
        let responseExpectation = self.expectation(description: "itunes search")
        var responseError: Error?
        
        interactor.search(term: "jack") { (_ , error) in
            
            responseError = error
            responseExpectation.fulfill()
            
        }
        
        self.waitForExpectations(timeout: 60.0) { (error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertNil(responseError, responseError!.localizedDescription)
            
            XCTAssertNotNil(self.presenter.results, "should return empty if there is no items with this term or there not exist in the data manager")
            
        }
        
    }
    
    func testOfflineSearch() {
        
        interactor.offlineSearch(term: "jack") { (resutls, error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertNotNil(resutls, "should return empty if there is no items with this term or there not exist in the data manager")
            
        }
        
    }
    
    func testFetchNext20() {
        
        interactor.presenter?.results = nil
        
        let responseExpectation = self.expectation(description: "itunes search")
        var responseError: Error?
        var results: SearchResults?
        
        interactor.search(term: "jack") { ( response , error) in
            
            responseError = error
            results = response
            responseExpectation.fulfill()
            
        }
        
        self.waitForExpectations(timeout: 60.0) { (error) in
            
            XCTAssertNil(error, error!.localizedDescription)
            
            XCTAssertNil(responseError, responseError!.localizedDescription)
            
            XCTAssertNotNil(results, "should return empty if there is no items with this term or there not exist in the data manager")
            
        }
        
    }
    
    func getOldSearchs() {
        
        dataManager.removeAllSearchs()
        dataManager.createMockData()
        
        let results = interactor.getOldSearchs()
        
         XCTAssertNotNil(results, "should return empty if there is no items with this term or there not exist in the data manager")
        
        XCTAssert(results!.count > 0, "should retrieve data saved correctly")
    }
    
    func saveOldSearchs() {
        
        dataManager.removeAllSearchs()
        
        let randomString = UUID().uuidString
        
        let result = interactor.saveSearchString(text: randomString)
        
        XCTAssertNotNil(result, "this should not fail")
        
        XCTAssertTrue(result!, "should retrieve data saved correctly")
        
    }
    
}
