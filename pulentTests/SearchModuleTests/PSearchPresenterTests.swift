//
//  PSearchPresenterTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PSearchPresenterTests: XCTestCase {
    
    class MockInteractor: PSearchInteractor {
        
        var searchCalled = false
        var fetchNext20Called = false
        var saveSearchStringCalled = false
        
        override func getOldSearchs() -> [String]? {
            return ["some string"]
        }
        
        override func search(term: String, completion: ((SearchResults?, Error?) -> ())? = nil) {
            
            searchCalled = true
            
            let searchResults = SearchResults(resultCount: 0, results: [])
            
            presenter?.showResults(results: searchResults)
            
            completion?(searchResults, nil)
            
        }
        
        override func fetchNext20(limit: Int, completion: ((SearchResults?, Error?) -> ())? = nil) {
            
            fetchNext20Called = true
            
            let searchResults = SearchResults(resultCount: 0, results: [])
            
            presenter?.showResults(results: searchResults)
            
            completion?(searchResults, nil)
            
        }
        
        override func saveSearchString(text: String) -> Bool? {
            
            saveSearchStringCalled = true
            
            return true
            
        }
        
    }
    
    class MockUserInterface: PSearchViewInterface {
        
        var chageSearchBarTextCalled = false
        var populatedCalled = false
        
        func chageSearchBarText(text: String) {
            
            chageSearchBarTextCalled = true
            
        }
        
        func populateView(with items: Array<RowItem>) {
            populatedCalled = true
        }
        
    }
    
    class MockWireframe: PSearchWireframe {
        
        var presentDetailsCalled = false
        
        override func presentDetailsScreen() {
            presentDetailsCalled = true
        }
        
    }
    
    let presenter = PSearchPresenter()
    let interactor = MockInteractor()
    let userInterface = MockUserInterface()
    let wireframe = MockWireframe()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        presenter.interactor = interactor
        presenter.userInterface = userInterface
        presenter.wireframe = wireframe
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCleanView() {
        
        userInterface.populatedCalled = false
        
        presenter.cleanView()
        
        XCTAssertNil(presenter.results, "this value should be nil")
        
        XCTAssertTrue(userInterface.populatedCalled, "PSearchViewInterface should be called clean all rows")
        
    }
    
    func testOldSearchs() {
        
        presenter.results = nil
        
        if AppDelegate.instance().isReachable() == false {
            
            presenter.showOldSearchs()
            
            XCTAssertTrue(userInterface.populatedCalled, "this method should be called after retrieve old search texts")
            
        } else {
            
            XCTAssertTrue(true, "no applicated")
            
        }
        
    }
    
    func testSearchBarTextDidEndEditing() {
        
        interactor.saveSearchStringCalled = false
        
        presenter.results = SearchResults(resultCount: 1, results: [])
        
        presenter.searchBarTextDidEndEditing(text: "some text")
        
        XCTAssertTrue(interactor.saveSearchStringCalled, "this method should be called after ending a succefully search")
        
    }
    
    func testAddResults() {
        
        userInterface.populatedCalled = false
        presenter.results = nil
        
        interactor.fetchNext20(limit: 100)
        
        XCTAssertTrue(presenter.results == nil, "addResults function should be called after fetching 20 next values")
    }
    
    func testShowResults() {
        
        userInterface.populatedCalled = false
        presenter.results = nil
        
        interactor.search(term: "jack")
        
        XCTAssertNil(presenter.results, "showResults function should be called after a search was excecuted")
    }
    
    func textSearchTextDidChange() {
        
        userInterface.populatedCalled = false
        presenter.results = nil
        
        presenter.searchTextDidChange(text: "ja")
        
        XCTAssertNil(presenter.results, "should be null if search text characters are less than 3")
        
        presenter.searchTextDidChange(text: "jack")
        
        XCTAssertNotNil(presenter.results, "after a successfully search this value should be changed")
        
    }
    
    func testInCurrentCellIndex() {
        
        interactor.fetchNext20Called = false
        presenter.results = SearchResults(resultCount: 10, results: [])
        
        presenter.inCurrentCellIndex(index: 9)
        
        XCTAssertTrue(interactor.fetchNext20Called, "after a successfully search this value should be changed")
    }
    
    func testDidSelectRowAt() {
        
        wireframe.presentDetailsCalled = false
        
        var item = SearchItem()
        item.trackId = Int(arc4random_uniform(8))
        
        let searchResults = SearchResults(resultCount: 1, results: [item])
        
        presenter.results = searchResults
        
        presenter.didSelectRowAt(indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(wireframe.presentDetailsCalled, "this value should changed if item at this index exists")
        
    }
    
}
