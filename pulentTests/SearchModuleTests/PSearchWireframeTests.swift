//
//  PSearchWireframeTests.swift
//  pulentTests
//
//  Created by @Ale on 7/28/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
@testable import pulent

class PSearchWireframeTests: XCTestCase {
    
    let wireframe = PSearchWireframe()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPresentSelfAsRootViewController() {
        
        wireframe.presentSelfAsRootViewController()
        
         XCTAssertNotNil(wireframe.viewController, "viewController should not be nill")
    }
    
}
