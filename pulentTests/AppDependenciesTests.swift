//
//  AppDependenciesTests.swift
//  pulentTests
//
//  Created by @Ale on 7/27/18.
//  Copyright © 2018 @Ale. All rights reserved.
//

import XCTest
import UIKit
@testable import pulent

class AppDependenciesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: Test configureDependencies
    
    func testThatShowsASearchViewController() {
        
        let appDependencies = AppDependencies()
        
        let window = UIWindow()
        let navigationController = MainNavigationController()
        window.rootViewController = navigationController
        
        appDependencies.configureDependencies(with: window)
        
        if let rootViewController = window.rootViewController as? MainNavigationController {
            
            if let searchViewController = rootViewController.viewControllers.first {
                
                XCTAssertNotNil(searchViewController as? PSearchViewController, "must be a search view controller")
                
            } else {
                
                XCTFail("first view controller must be a view controller")
                
            }
            
        } else {
            
            XCTFail("window must have a rootViewController that is a navigation controller")
            
        }
    }
    
}
